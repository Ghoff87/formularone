import {environment} from 'environments/environment';
import {HttpHeaders} from '@angular/common/http';

export const AppConfig = {
  apiBaseUrl: environment.ApiBaseUrl
};

export const F1_headers = new HttpHeaders();