import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailsComponent} from './details/details.component';
import { SeasonResolver } from './services/season.resolver';


const seasonRoutes: Routes = [
  {
    path: 'season', 
    children: [
      {path: ':year', resolve: {season: SeasonResolver}, component: DetailsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(seasonRoutes)],
  exports: [RouterModule]
})
export class SeasonRoutingModule {
}
