import { Component, OnInit } from '@angular/core';
import { SeasonService } from 'app/core/services/season.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  public year = null;
  public seasonRaces = null;
  public season = null;
  public winner = null;

  constructor(private seasonService: SeasonService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.year = this.route.snapshot.paramMap.get('year');

    if(this.year){
      this.getRaceInformations();
    }
  }

  /**
   * Get season Informations
   */
  private getRaceInformations() {
    this.route.data.subscribe(response => {
      this.seasonRaces = response.season[0].MRData.RaceTable.Races;
      this.season = response.season[0].MRData.RaceTable.season;
      this.winner = response.season[1].MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
    });
  }
}
