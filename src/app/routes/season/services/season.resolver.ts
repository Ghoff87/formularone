import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable, forkJoin} from 'rxjs';
import {SeasonService} from 'app/core/services/season.service';

@Injectable()
export class SeasonResolver implements Resolve<any> {
  constructor(private seasonService: SeasonService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {

    let year = route.params['year'];
    let filter = {'limit': '1'};

    if (year) {
      return forkJoin([
        this.seasonService.getSeasonByYear(route.params['year']),
        this.seasonService.getDriverStandings(filter, year)
      ])
    } else {
      this.router.navigateByUrl('/error404');
    }

  }
}
