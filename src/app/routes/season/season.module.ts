import { NgModule } from '@angular/core';
import { DetailsComponent } from './details/details.component';
import { SeasonRoutingModule } from './season-routing.module';
import { SeasonService } from 'app/core/services/season.service';
import { SeasonResolver } from './services/season.resolver';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [SeasonRoutingModule, CommonModule],
    declarations: [DetailsComponent],
    providers: [SeasonService, SeasonResolver],
})

export class SeasonModule {
    
}
