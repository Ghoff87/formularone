import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './routes';
import { HomeModule } from './home/home.module';
import { SeasonModule } from './season/season.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HomeModule,
    SeasonModule,
    CommonModule
  ],
  declarations: [],
  exports: [
    RouterModule,
  ]
})

export class RoutesModule {
}
