import { Component, OnInit } from '@angular/core';
import { SeasonService } from 'app/core/services/season.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public filter = {'limit': '11', 'offset': '55'};
  public seasons: any = [];

  constructor(private seasonSerice: SeasonService) {}

  ngOnInit() {
    this.loadSeasons();
  }

  loadSeasons() {
    this.seasonSerice.getSeasons(this.filter).subscribe((result: any) => {
      this.seasons = result.MRData.StandingsTable.StandingsLists;
    });
  }
}
