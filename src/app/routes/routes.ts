import {Routes} from '@angular/router';
import {HomeComponent} from './home/home/home.component';
import { Error500Component } from 'app/core/error500/error500.component';
import { Error404Component } from 'app/core/error404/error404.component';

export const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'season',
    loadChildren: './season/season.module#SeasonModule'
  },

  // Not found
  {path: 'error500', data: {layout: 'void'}, component: Error500Component},
  {path: '**', data: {layout: 'void'}, component: Error404Component}
  
];
