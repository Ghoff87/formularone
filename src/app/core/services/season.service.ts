import {Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig, F1_headers} from 'app/config';

const headers = F1_headers;

headers['Content-Type'] = 'application/json';

@Injectable()
export class SeasonService {

  private httpOptions = {params: {}, headers: headers};

  /**
   * Auth service constructor
   *
   * @param {HttpClient} http
   */
  constructor(private http: HttpClient, private injector: Injector) {
  }

  /**
   * Get news total count
   *
   * @param {} filter
   */
  getSeasons(filter: any) {
    this.httpOptions.params = filter;

    return this.http.get(`${AppConfig.apiBaseUrl}/f1/driverStandings/1.json`, this.httpOptions);
  }

  /**
   * Get News By year
   *
   * @param any year
   */
  getSeasonByYear(year: any) {
    this.httpOptions.params = {};
    
    return this.http.get(`${AppConfig.apiBaseUrl}/f1/${year}/results/1.json`, this.httpOptions);
  }

  /**
   * Get News By year
   *
   * @param {} filter
   * @param any year
   */
  getDriverStandings(filter: any, year: any) {
    this.httpOptions.params = filter;
    
    return this.http.get(`${AppConfig.apiBaseUrl}/f1/${year}/driverStandings.json`, this.httpOptions);
  }

}
