import { NgModule, Optional, SkipSelf } from '@angular/core';

import {Error404Component} from './error404/error404.component';
import {Error500Component} from './error500/error500.component';

@NgModule({
    imports: [],
    providers: [],
    declarations: [
      Error404Component,
      Error500Component
    ],
    exports: []
})
export class CoreModule {
}
